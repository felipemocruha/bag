package tracing

import (
	"fmt"

	"github.com/labstack/echo"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/uber/jaeger-client-go"
	jaegerprom "github.com/uber/jaeger-lib/metrics/prometheus"
)

func NewJaegerTracer(prefix, host string) (opentracing.Tracer, func(), func(), error) {
	factory := jaegerprom.New()
	metrics := jaeger.NewMetrics(factory, map[string]string{"lib": "jaeger"})
	sampler := jaeger.NewRateLimitingSampler(2)

	transport, err := jaeger.NewUDPTransport(host, 0)
	if err != nil {
		return nil, nil, nil, err
	}

	reporter := jaeger.NewCompositeReporter(
		jaeger.NewRemoteReporter(transport,
			jaeger.ReporterOptions.Metrics(metrics),
		),
	)
	rcloser := func() { reporter.Close() }

	tracer, closer := jaeger.NewTracer(prefix,
		sampler,
		reporter,
		jaeger.TracerOptions.Metrics(metrics),
	)
	tcloser := func() { closer.Close() }
	opentracing.SetGlobalTracer(tracer)

	return tracer, rcloser, tcloser, nil
}

func TracingEchoMiddleware(prefix string, tracer opentracing.Tracer) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			var span opentracing.Span
			opName := fmt.Sprintf("%v:%v", prefix, ctx.Path())
			req := ctx.Request()

			wireContext, err := opentracing.GlobalTracer().Extract(
				opentracing.TextMap,
				opentracing.HTTPHeadersCarrier(req.Header))
			if err != nil {
				span = opentracing.StartSpan(opName)
			} else {
				span = opentracing.StartSpan(opName, opentracing.ChildOf(wireContext))
			}
			defer span.Finish()

			ext.Component.Set(span, prefix)
			ext.SpanKind.Set(span, "server")
			ext.HTTPMethod.Set(span, req.Method)
			ext.HTTPUrl.Set(span, req.URL.String())
			ext.PeerHostname.Set(span, req.URL.Host)

			if err := next(ctx); err != nil {
				ext.Error.Set(span, true)
				ctx.Error(err)
			}
			ext.Error.Set(span, false)
			ext.HTTPStatusCode.Set(span, uint16(ctx.Response().Status))

			return nil
		}
	}
}
