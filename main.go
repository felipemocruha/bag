package main

import (
	"errors"
	"github.com/labstack/echo"
	"gitlab.com/felipemocruha/bag/metrics"
	"gitlab.com/felipemocruha/bag/tracing"	
)

func main() {
	tracer, r, t, _ := tracing.NewJaegerTracer("test", "0.0.0.0:5775")
	defer r()
	defer t()
	
	srv := metrics.NewEchoServer("test")
	srv.GET("/healthcheck", func(c echo.Context) error {
		return nil
	})

	srv.GET("/foo/:id/bar/:baz", func(c echo.Context) error {
		return errors.New("haha")
	})
	
	srv.Use(tracing.TracingEchoMiddleware("test", tracer))
	go metrics.RunMetricsServer("0.0.0.0:8091")
	srv.Start("0.0.0.0:8090")
}
