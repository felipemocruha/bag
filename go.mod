module gitlab.com/felipemocruha/bag

go 1.12

require (
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/opentracing/opentracing-go v1.1.0
	github.com/prometheus/client_golang v1.1.0
	github.com/rs/zerolog v1.15.0
	github.com/stretchr/testify v1.4.0
	github.com/uber/jaeger-client-go v2.18.1+incompatible
	github.com/uber/jaeger-lib v2.1.1+incompatible
	gopkg.in/yaml.v2 v2.2.2
)
