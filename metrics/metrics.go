package metrics

import (
	"net/http"

	"github.com/labstack/echo"
	"github.com/rs/zerolog/log"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// RunMetricsServer exposes a server with "/metrics" route in prometheus format.
// Logging is enabled by default.
func RunMetricsServer(addr string, loggingEnabled ...bool) {
	http.Handle("/metrics", promhttp.Handler())

	if len(loggingEnabled) == 0 || loggingEnabled[0] {
		log.Info().Msgf("Starting metrics server at: %v/metrics", addr)
		log.Error().Msg(http.ListenAndServe(addr, nil).Error())
	}

	http.ListenAndServe(addr, nil)
}

// NewEchoServer returns an echo.Server with opinionated defaults
func NewEchoServer(prefix string) *echo.Echo {
	server := echo.New()
	server.HideBanner = true
	server.HidePort = true
 	server.Use(HttpMetricsEchoMiddleware(prefix))

	return server
}

