package metrics

import (
	"time"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/labstack/echo"
)

func HttpMetricsEchoMiddleware(prefix string) echo.MiddlewareFunc {
	labels := []string{"method", "path", "status_code"}

	requestCount := promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: prefix,
		Name:      "http_requests_total",
		Help:      "Requests count by method/path/status.",
	}, labels)

	requestDurations := promauto.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: prefix,
		Name:      "http_request_duration_seconds",
		Help:      "Request time by method/path/status.",
	}, labels)

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			start := time.Now()
			if err := next(c); err != nil {
				c.Error(err)
			}

			metrics := []string{
				c.Request().Method,
				c.Path(),
				strconv.Itoa(c.Response().Status),
			}

			requestCount.WithLabelValues(metrics...).Inc()
			requestDurations.
				WithLabelValues(metrics...).
				Observe(time.Since(start).Seconds())

			return nil
		}
	}
}
